# symfony/browser-kit

https://packagist.org/packages/symfony/browser-kit

[![PHPPackages Rank](http://phppackages.org/p/symfony/browser-kit/badge/rank.svg)](http://phppackages.org/p/symfony/browser-kit)
[![PHPPackages Referenced By](http://phppackages.org/p/symfony/browser-kit/badge/referenced-by.svg)](http://phppackages.org/p/symfony/browser-kit)

## Official documentation
* [*The BrowserKit Component*
  ](https://symfony.com/doc/current/components/browser_kit.html)

## Unofficial documentation
* [*You may have memory leaking from PHP 7 and Symfony tests*
  ](https://jolicode.com/blog/you-may-have-memory-leaking-from-php-7-and-symfony-tests)
  2019-10 Damien Alexandre